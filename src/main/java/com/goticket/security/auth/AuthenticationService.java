package com.goticket.security.auth;

import com.goticket.security.component.Utils;
import com.goticket.security.config.JwtService;
import com.goticket.security.user.Role;
import com.goticket.security.user.User;
import com.goticket.security.user.UserMapper;
import com.goticket.security.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
//@RequiredArgsConstructor
public class AuthenticationService {



    private final UserRepository repository;

    public AuthenticationService(@Qualifier("userRepo") UserRepository repository) {
        this.repository = repository;
    }

    @Autowired
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);
    @Autowired
    private JwtService jwtService;
    private AuthenticationManager authenticationManager;

    @Autowired
    private Utils utils;

    public AuthenticationResponse register(RegisterRequest request) {
        var user = User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .phone(request.getPhone())
                .email(request.getEmail())
                .password(request.getPassword())
                .userId(utils.generateUserId(32))
                .role(Role.USER)
                .build();
        repository.save(userMapper.userToEntity(user));
        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }

    public AuthenticationResponse authentication(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        var user = repository.findByEmail(request.getEmail()).orElseThrow();
        var jwtToken = jwtService.generateToken(userMapper.userEntityToUser(user));
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }
}
