package com.goticket.security.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/demo-contoller")
public class DemoController {

    @GetMapping
    public ResponseEntity<String> sayHelo(){
        return ResponseEntity.ok("Hello from endpoint");
    }
}
