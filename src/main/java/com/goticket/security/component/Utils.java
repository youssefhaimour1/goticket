package com.goticket.security.component;

import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.Random;

@Component
public class Utils {
    private final Random RANDOM = new SecureRandom();
    private final String ALPHANUMERIQUE = "0123456789AZERTYUIOPQSDFGHJKLMWXCVBNazertyuiopmlkjhgfdsqwxcvbn";
    public String generateUserId(int lenght){
        StringBuilder returnValue = new StringBuilder(lenght);
        for (int i=0 ; i < lenght;i++){
            returnValue.append(ALPHANUMERIQUE.charAt(RANDOM.nextInt(ALPHANUMERIQUE.length())));
        }
        return new String(returnValue);
    }
}
