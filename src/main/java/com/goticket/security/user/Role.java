package com.goticket.security.user;

public enum Role {
    USER,
    ADMIN,
    ORGANISATOR
}
