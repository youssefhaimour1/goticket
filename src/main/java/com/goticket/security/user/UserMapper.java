package com.goticket.security.user;

import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    UserEntity userToEntity(User user);
    User userEntityToUser(UserEntity userEntity);
    List<User> userEntitiesToUsers(List<UserEntity> userEntityList);
}
